const SdataN = [
    {
        name: 'Sword Of Destiny',
        imgSrc: 'https://www.indiewire.com/wp-content/uploads/2017/09/crouching-tiger-hidden-dragon-sword-of-destiny-2016.jpg?w=675',
        rating: '⭐ 8.5',
        link: 'https://www.netflix.com/in/title/80100172?source=35',
        desc: 'The Gray Man is a 2022 American action thriller film directed by Anthony and Joe Russo, from a screenplay the latter co-wrote with Christopher Markus'
    },
    {
        name: 'The Gray Man',
        imgSrc: 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTCR9FSMDFVM4V_l6fcJRssT-oPvAyDpjqAxZWYTyySqKh7RgLL',
        rating: '⭐ 8.5',
        link: 'https://www.netflix.com/in/title/81160697?source=35',
        desc: 'The Gray Man is a 2022 American action thriller film directed by Anthony and Joe Russo, from a screenplay the latter co-wrote with Christopher Markus'
    },
    {
        name: 'Extraction',
        imgSrc: 'https://m.media-amazon.com/images/M/MV5BYWZmM2UyOTEtMGJjZC00ODYyLWJmYzgtYmU0Nzk0MDA5NjM2XkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_FMjpg_UX1000_.jpg',
        rating: '⭐ 8.5',
        link: 'https://www.netflix.com/in/title/81277950?source=35',
        desc: 'The Gray Man is a 2022 American action thriller film directed by Anthony and Joe Russo, from a screenplay the latter co-wrote with Christopher Markus'
    },
    {
        name: 'Stranger Things',
        imgSrc: 'https://images-na.ssl-images-amazon.com/images/M/MV5BMjEzMDAxOTUyMV5BMl5BanBnXkFtZTgwNzAxMzYzOTE@._V1_.jpg',
        rating: '⭐ 8.5',
        link: 'https://www.netflix.com/in/title/81277950?source=35',
        desc: 'The Gray Man is a 2022 American action thriller film directed by Anthony and Joe Russo, from a screenplay the latter co-wrote with Christopher Markus'
    },
]

export default SdataN